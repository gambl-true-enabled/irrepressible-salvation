package com.hurenamy.presalvat

import android.content.Context
import android.webkit.JavascriptInterface

private const val SALVATION_TABLE = "com.SALVATION.table"
private const val SALVATION_ARGS = "com.SALVATION.value"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(SALVATION_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(SALVATION_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(SALVATION_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(SALVATION_ARGS, null)
}
